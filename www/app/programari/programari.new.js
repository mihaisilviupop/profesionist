angular.module('profesionist.controllers')

    .controller('ProgramariNewCtrl', function ($scope, $rootScope, $stateParams, Afaceri, Programari, $q, $state, $ionicListDelegate, $ionicLoading, currentAuth, currentAfacere, $firebaseArray, $firebaseObject, $ionicPopup, $ionicPopover, $timeout) {
        $scope.weekday = ["duminica", "luni", "marti", "miercuri", "joi", "vineri", "sambata"]
        $scope.idAfacere = $stateParams.idAfacere;
        $scope.numeView = "Programari pentru " + currentAfacere.nume;
        $scope.afacere = currentAfacere;
        $scope.selectedDate = {
            "date": new Date(),
            "start": "",
            "stop": ""
        }
        $scope.allHours = [];
        var fbObjDataAleasa;
        var afaceriReference;

        function getProgramari(data) {
            var dataCrt = new Date(data);//data selectata, obiect js Date 
            var dataFormat = dataCrt.getDate() + "-" + (dataCrt.getMonth() + 1) + "-" + dataCrt.getFullYear();//data in formatul DD-MM-YYYY
            /**referinta firebase
             *      colectia rezervari
             *          copilul al carui nume sa fie identificatorul afacerii curente($stateParams.idAfacere)
             *              copil al carui nume corespunde cu dataFormat */
            afaceriReference = firebase.database().ref("rezervari").child($stateParams.idAfacere).child(dataFormat);
            /**obiectul din referinta specificata
             * daca nu exista un obiect cu informatii la acea referinta se va creea unul local
             */
            var obj = $firebaseObject(afaceriReference);
            //callback pentru atunci cand se termina de adus datele din cloud
            obj.$loaded().then(function () {
                //  console.log("loaded record:", obj);

                // // To iterate the key/value pairs of the object, use angular.forEach()
                // angular.forEach(obj, function (value, key) {
                //     console.log(key, value);
                // });
                obj;
                // if (!obj[dataFormat])
                //     obj[dataFormat] = "emrge2?"
                if (fbObjDataAleasa)
                    fbObjDataAleasa.$destroy();
                obj;//obiectul adus din bd sau cel creat local
                /**daca exista un obiect de tipul $firebaseObject adus se va apela metoda remove(), ca sa nu se suprapuna cu datele noi
                 * si sa nu se salveze date duplicate, astfel renuntanduse la modificarile nesalvate dintrun obiect
                 */
                // if (fbObjDataAleasa) {
                //     fbObjDataAleasa.$remove().then(function (ref) {
                //     }, function (error) {
                //         console.log("Error:", error);
                //     });
                // }
                fbObjDataAleasa = obj;

                //  fbObjDataAleasa["10:30"]["status"] = "refuzat";
                // fbObjDataAleasa = {
                //     "10:30": {
                //         "status": "refuzat"
                //     }
                // }
                // fbObjDataAleasa.$save().then(function (ref) {
                //     console.log("save succ", ref);
                // }, function (error) {
                //     console.log("Error:", error);
                // });

                /**sinconizez array-urile cu lista orelor si disonibilitatea lor
                 * in fiecare obiect din array-ul $scope.allHours introduc o noua proprietare cu statusul programarii corespunzatoare
                 */
                syncArrays(fbObjDataAleasa);
                // pentru three-way data bindings, leg obiectul de scope
                obj.$bindTo($scope, "fbObjDataAleasa");
            });
        };

        /**
         * caut selected===true din $scope.allHours 
         * caut in fbObjDataAleasa daca are proprietate cu oraStart de la selected
         * daca are modific 
         * daca nu adaug 
         * corelez propietate fbObjDataAleasa cu obiect din array
         * 
         * 8:30-9:00
         *	    status: "acceptat"
         *      user:{  
         *          nume: "nume"
         *          uid:  "id"
         *      }
         * status:""
         * hour: data
         * id: 2
         * idNou:12:45
         * selected:true
         */
        $scope.setProgramari = function () {
            var solutie = [];
            var dataCrt = new Date($scope.selectedDate.date);
            var dataFormat = dataCrt.getDate() + "-" + (dataCrt.getMonth() + 1) + "-" + dataCrt.getFullYear();
            var confirm = false;

            for (var i = 0; i < $scope.allHours.length; i++)
                if ($scope.allHours[i].selected && (!$scope.allHours[i]["user"].status || $scope.allHours[i]["user"].status != "acceptat")) {
                    solutie.push($scope.allHours[i]);
                }

            if (solutie.length > 0)
                for (var i = 0; i < solutie.length; i++)
                    if (solutie[i].status === "asteptare") {
                        confirm = true;
                        break;
                    }
            if (confirm) {
                var confirmPopup = $ionicPopup.confirm({
                    title: 'Atenție, rezervări în așteptare!',
                    template: 'Ora selectată are rezervări în așteptare făcute de alți utilizatori! Este posibil ca rezervarea dv. sa nu fie acceptata! Doriti sa continuati?'
                });
                confirmPopup.then(function (res) {
                    if (res) {
                        save();
                    } else {
                        return;
                    }
                });
            }
            else
                save();


            function save() {
                for (var i = 0; i < solutie.length; i++) {
                    // console.log(afaceriReference, solutie, solutie[i]["idNou"]);
                    var idNou = solutie[i]["idNou"];
                    var solutieFinal = solutie;
                    var index = i;
                    console.log("idNou", idNou);
                    console.log("afaceriReference", afaceriReference + "");
                    afaceriReference.once("value").then((snapshot) => {
                        if (snapshot) {
                            //console.log(snapshot, solutie, solutie[i].idNou);
                            console.log("solutie idNou", idNou);

                            if (snapshot.exists() && snapshot.child(idNou).exists()) {
                                if (snapshot.child(idNou + "/user")) {
                                    //push sau update
                                    var users = fbObjDataAleasa[idNou]["user"];
                                    for (var j = 0; j < users.length; j++) {
                                        if (users[j]["uid"] === solutieFinal[index]["user"]["uid"]) {
                                            fbObjDataAleasa[idNou]["user"][j]["nume"] = $rootScope.userData.displayName;
                                            fbObjDataAleasa[idNou]["user"][j]["uid"] = currentAuth.uid;
                                            fbObjDataAleasa[idNou]["user"][j]["status"] = "asteptare";
                                            var update = true;
                                            break;
                                        }
                                    }
                                    if (!update) {
                                        fbObjDataAleasa[idNou]["user"].push({
                                            "nume": $rootScope.userData.displayName,
                                            "uid": currentAuth.uid,
                                            "status": "asteptare"
                                        })
                                    }

                                } else {
                                    fbObjDataAleasa[idNou] = {
                                        "user": [{
                                            "nume": $rootScope.userData.displayName,
                                            "uid": currentAuth.uid,
                                            "status": "asteptare"
                                        }]
                                    };
                                }
                            }
                            else {
                                fbObjDataAleasa[idNou] = {
                                    "user": [{
                                        "nume": $rootScope.userData.displayName,
                                        "uid": currentAuth.uid,
                                        "status": "asteptare"
                                    }]
                                };
                                //     fbObjDataAleasa = {
                                //         idNou + "":{

                                //     }
                                //     // idNou:{

                                //     // }
                                // }
                                // fbObjDataAleasa = {
                                //     "user": [{
                                //         "nume": $rootScope.userData.displayName,
                                //         "uid": currentAuth.uid,
                                //         "status": "asteptare"
                                //     }]
                                // };
                            }
                        }
                        fbObjDataAleasa.$save().then(function (ref) {
                            console.log(ref, fbObjDataAleasa);
                        }, function (error) {
                            console.log("Error:", error);
                        });
                    });


                }
            }


        }

        function itemInArray(array, item) {
            for (var i = 0; i < array.length; i++)
                if (array[i].uid === item.uid)
                    return true;
            return false;
        }


        function deleteItem(item) {
            if (item.user && item.user.uid === currentAuth.uid && fbObjDataAleasa[item.idNou]["user"] && angular.isArray(fbObjDataAleasa[item.idNou]["user"])) {
                for (var i = 0; i < fbObjDataAleasa[item.idNou]["user"].length; i++)
                    if (item.user.uid === fbObjDataAleasa[item.idNou]["user"][i].uid)
                        fbObjDataAleasa[item.idNou]["user"][i].status = "anulat";
            }
            fbObjDataAleasa.$save().then(function (ref) {
            }, function (error) {
                console.log("Error:", error);
            });
        }

        /**
         * vf ziua staptamanii a datei alese
         * vf daca in ziua respectiva este orar de functionare
         * iau ora start si ora stop
         * aduc programarie din ziua respectiva
         */
        $scope.$watch('selectedDate.date', function (newValue, oldValue) {
            if (($scope.typeOfAuth() === 0 && new Date(newValue).setHours(0, 0, 0, 0) >= new Date().setHours(0, 0, 0, 0)) || $scope.typeOfAuth() === 1) {
                if ($scope.afacere.orar[$scope.weekday[newValue.getDay()]] && $scope.afacere.orar[$scope.weekday[newValue.getDay()]].start && $scope.afacere.orar[$scope.weekday[newValue.getDay()]].stop) {
                    if ($scope.afacere.orar[$scope.weekday[newValue.getDay()]].start.indexOf(":") > -1 && $scope.afacere.orar[$scope.weekday[newValue.getDay()]].stop.indexOf(":") > -1) {
                        prepareHourArray(newValue, $scope.afacere.orar[$scope.weekday[newValue.getDay()]].start, $scope.afacere.orar[$scope.weekday[newValue.getDay()]].stop);
                        getProgramari(newValue);
                    }
                    $scope.error = undefined;
                } else {
                    $scope.allHours = [];
                    $scope.error = "Ziua aleasă nu are program!"
                }
            }
            else {
                alert("Data aleasă este incorectă")
                $scope.allHours = [];
                $scope.selectedDate.date = oldValue;
            }
        });

        $scope.onSelected = function (item) {
            for (var i = 0; i < $scope.allHours.length; i++) {
                if ($scope.allHours[i]["id"] === item["id"]) {
                    if ($scope.typeOfAuth() === 1) {

                    }
                    else if ($scope.typeOfAuth() === 0) {
                        if ($scope.allHours[i].selected) {
                            $scope.allHours[i].selected = false;
                            $scope.allHours[i].user = undefined;
                        } else {
                            //  $scope.allHours[i].selected = true;
                            $scope.allHours[i].user = {
                                "status": "asteptare",
                                "nume": $rootScope.userData.displayName,
                                "uid": currentAuth.uid
                            };
                            doConfirm($scope.allHours[i]);
                        }
                    }
                }
            }
            function doConfirm(item) {
                $ionicPopup.confirm({
                    title: 'Rezervare nouă',
                    template: 'Doriți să faceți o rezervare la ora selectată?'
                }).then(function (res) {
                    if (res) {
                        saveChanges(item);
                    } else {
                        console.log('You are not sure');
                    }
                });
            }
        }


        function saveChanges(item) {
            var idNou = item["idNou"];
            afaceriReference.once("value").then((snapshot) => {
                if (snapshot) {
                    if (snapshot.exists() && snapshot.child(idNou).exists()) {
                        if (snapshot.child(idNou + "/user")) {
                            //push sau update
                            var users = fbObjDataAleasa[idNou]["user"];
                            for (var j = 0; j < users.length; j++) {
                                if (users[j]["uid"] === item["user"]["uid"]) {
                                    fbObjDataAleasa[idNou]["user"][j]["nume"] = $rootScope.userData.displayName;
                                    fbObjDataAleasa[idNou]["user"][j]["uid"] = currentAuth.uid;
                                    fbObjDataAleasa[idNou]["user"][j]["status"] = "asteptare";
                                    var update = true;
                                    break;
                                }
                            }
                            if (!update) {
                                fbObjDataAleasa[idNou]["user"].push({
                                    "nume": $rootScope.userData.displayName,
                                    "uid": currentAuth.uid,
                                    "status": "asteptare"
                                })
                            }

                        } else {
                            fbObjDataAleasa[idNou] = {
                                "user": [{
                                    "nume": $rootScope.userData.displayName,
                                    "uid": currentAuth.uid,
                                    "status": "asteptare"
                                }]
                            };
                        }
                    }
                    else {
                        fbObjDataAleasa[idNou] = {
                            "user": [{
                                "nume": $rootScope.userData.displayName,
                                "uid": currentAuth.uid,
                                "status": "asteptare"
                            }]
                        };
                    }
                }
                fbObjDataAleasa.$save().then(function (ref) {
                    console.log(ref, fbObjDataAleasa);
                }, function (error) {
                    console.log("Error:", error);
                });
            });


        }

        function prepareHourArray(data, oraDeschidere, oraInchidere) {
            if (($scope.typeOfAuth() === 0 && new Date(data).setHours(0, 0, 0, 0) >= new Date().setHours(0, 0, 0, 0)) || $scope.typeOfAuth() === 1) {
                oraDeschidere = oraDeschidere.split(":");
                oraInchidere = oraInchidere.split(":");
                var ora = {
                    "deschidere": {
                        "h": oraDeschidere[0],
                        "m": oraDeschidere[1]
                    },
                    "inchidere": {
                        "h": oraInchidere[0],
                        "m": oraInchidere[1]
                    }
                }
                var allHoursTemp = [];
                var id = 1;
                var oraStart = new Date(data.setHours(ora.deschidere.h, ora.deschidere.m, 0, 0));
                var oraStop = new Date(data.setHours(ora.inchidere.h, ora.inchidere.m, 0, 0));
                var pas = 1800000; //30 min in ms

                allHoursTemp.push({ "id": id, "idNou": setId(new Date(oraStart)), "hour": new Date(oraStart), "selected": false });
                // while (new Date(oraTemp).getTime() < new Date(oraStop).getTime() && new Date(oraStop).getTime() - new Date(oraTemp).getTime() > 1800000) {
                //     oraTemp = new Date(oraTemp.setTime(oraTemp.getTime() + 1800000));
                //     id++;
                //     allHoursTemp.push({ "id": id, "hour": oraTemp, "selected": false, "avaible": true });
                // }
                while (new Date(oraStart).getTime() < new Date(oraStop).getTime() && new Date(oraStop).getTime() - new Date(oraStart).getTime() > pas) {
                    id++;
                    var dataWhile = new Date(oraStart.setTime(oraStart.getTime() + pas));
                    allHoursTemp.push({ "id": id, "idNou": setId(dataWhile), "hour": dataWhile, "selected": false });
                }
                id++;
                allHoursTemp.push({ "id": id, "idNou": setId(new Date(oraStop)), "hour": new Date(oraStop), "selected": false });
                $scope.allHours = allHoursTemp;
            }
            else {
                alert("Data aleasă este incorectă")
                $scope.selectedDate.date = new Date();
                $scope.allHours = [];
            }
        }

        /**
         * status: asteptare, acceptat, respins
         */
        $scope.whatClassIsIt = function (selected) {
            var stil = "";
            if (!selected.selected && selected.status === "asteptare")
                stil = "programare_waiting";
            else if (selected.status === "acceptat")
                stil = "programare_rejected";
            else if (selected.selected && (!selected.status || selected.status === "asteptare" || selected.status === "respins"))
                stil = "programare_success";
            else if (selected.selected && selected.status === "asteptare")
                stil = "programare_success";
            else if (selected.selected && (!selected.status || selected.status != "asteptare" && selected.status != "acceptat" && selected.status != "respins"))
                stil = "programare_success";
            else
                stil = "";
            if (selected.status === "acceptat" && selected.user && selected.user.uid === currentAuth.uid)
                stil = "programare_success";
            if (selected.user && selected.user.uid === currentAuth.uid) {
                stil += " bordura"
            }
            return stil;
        }
        /**vf daca o programare corespunde cu useru logat */
        $scope.itemStatus = function (item) {
            if (item.user && item.user.uid === currentAuth.uid)
                return true;
            return false;
        }

        $scope.$watch("fbObjDataAleasa", function (newValue, oldValue) {
            // console.log("watch fb", newValue, oldValue);
            syncArrays(newValue);
        });

        /**
         * fbObjDataAleasa
         * $scope.allHours
         * cand se schimba statusul unei proprietati din fbObjDataAleasa sync in $scope.allHours 
         * */
        function syncArrays(fbObj) {
            if ($scope.allHours.length < 2) return;
            //toate orele
            for (var i = 0; i < $scope.allHours.length; i++) {
                //orele din bd
                for (var prop in fbObj) {
                    if ($scope.allHours[i]["idNou"] === prop) {
                        $scope.allHours[i]["user"] = setCurrentUser(fbObj[prop]);
                        if ($scope.typeOfAuth() === 1) {
                            $scope.allHours[i]["user"] = fbObj[prop]["user"];
                            $scope.allHours[i]["status"] = setGeneralStatus(fbObj[prop]);
                        }
                        else
                            $scope.allHours[i]["status"] = $scope.allHours[i]["user"]["status"];
                        //  console.log($scope.allHours[i]["user"]);
                    }
                }
            }
        }


        /**useru daca are 
         * false daca nu
         */
        function setCurrentUser(fbObj) {
            if (!fbObj.user && angular.isArray(fbObj.user) === false) return undefined;

            for (var i = 0; i < fbObj.user.length; i++)
                if (fbObj.user[i].uid === currentAuth.uid) {
                    //console.log(fbObj.user[i]);
                    return fbObj.user[i];
                }
            return undefined;
        }

        /**daca un user are acceptat return acceptat 
         *  daca nu asteptare sau "" */
        function setGeneralStatus(item) {
            var statFin = "";
            if (!item.user) return statFin;
            if (angular.isArray(item.user) === true) {
                for (var i = 0; i < item.user.length; i++) {
                    if (item.user[i].status === "acceptat") {
                        statFin = "acceptat";
                        return statFin;
                    }
                    if (item.user[i].status === "asteptare" && statFin != "asteptare")
                        statFin = "asteptare";
                }
            } else {
                statFin = item.user.status;

            }
            return statFin;
        }



        function setId(date) {
            return new Date(date).getHours() + ":" + new Date(date).getMinutes();
        }

        $scope.openDetails = function (item) {
            $scope.currentItem = item;
            if ($scope.currentItem) {
                if ($scope.typeOfAuth() === 0) {
                    detailsForUser();
                }
                else if ($scope.typeOfAuth() === 1) {
                    if (item.user) {
                        detailsForProprietar();
                    }
                }
            }
        }

        $scope.consola = function (item) {
            console.log(item);
        }

        $scope.changeStatus = function (user) {
            user.status = user.toggleValue ? "acceptat" : "refuzat";
            for (var i = 0; i < $scope.currentItem.user.length; i++) {
                var element = $scope.currentItem.user[i];
                if (element.uid != user.uid) {
                    element.status = "refuzat";
                    element.toggleValue = false;
                }
            }
        }

        function detailsForUser() {
            $ionicPopup.show({
                templateUrl: 'app/programari/templates/detailsUser.html',
                cssClass: "popup",
                title: 'Programarea mea',
                subTitle: "Status: " + $scope.currentItem.status,
                scope: $scope,
                buttons: [
                    { text: 'Anulează' },
                    {
                        text: '<b>Salvează</b>',
                        type: 'button-positive',
                        onTap: function (e) {
                            if (!$scope.currentItem.delete) {
                                //don't allow the user to close unless he enters wifi password
                                e.preventDefault();
                            } else {
                                deleteItem($scope.currentItem);
                            }
                        }
                    },
                ]
            });
        }



        function detailsForProprietar() {
            console.log($scope.currentItem, $scope.currentItem["user"])

            $ionicPopup.show({
                templateUrl: 'app/programari/templates/detailsProprietar.html',
                cssClass: "popup",
                title: 'Programări ora ' + new Date($scope.currentItem.hour).getHours() + ":" + new Date($scope.currentItem.hour).getMinutes(),
                subTitle: new Date($scope.selectedDate.date).toDateString(),
                scope: $scope,
                buttons: [
                    { text: 'OK' },
                    // {
                    //     text: '<b>Salvează</b>',
                    //     type: 'button-positive',
                    //     onTap: function (e) {
                    //         if (!$scope.currentItem) {
                    //             //don't allow the user to close unless he enters wifi password
                    //             e.preventDefault();
                    //         } else {
                    //             deleteItem($scope.currentItem);
                    //         }
                    //     }
                    // },
                ]
            });
        }

        /** Retuurneaza tipul utilizatorului autentificat
         *      1 = proprietar afacere curenta
         *      0 = utilizator normal
         */
        $scope.typeOfAuth = function () {
            if ($scope.afacere.proprietar === currentAuth.uid)
                return 1;
            return 0;
        }

        $scope.disableDetailsBtn = function (item) {
            //typeOfAuth()===0 && !itemStatus(item) e true atunci true
            //typeOfAuth()===1 &&  item.user si  item.user e true atunci true
            //   console.log($scope.allHours)
            if ($scope.typeOfAuth() === 0 && $scope.itemStatus(item) === false)
                return true;
            if ($scope.typeOfAuth() === 1 && !item.user) {
                return true;
            }
            return false;
        }


        // An alert dialog
        $scope.showAlert = function () {
            var alertPopup = $ionicPopup.alert({
                title: 'Atenție!',
                template: 'Ora selectată are rezervări în așteptare făcute de alți utilizatori!'
            });
            alertPopup.then(function (res) {
                console.log('Thank you for not eating my delicious ice cream cone');
            });
        };

        $ionicPopover.fromTemplateUrl('app/programari/templates/popoverProgramari.html', {
            scope: $scope,
        }).then(function (popover) {
            $scope.popover = popover;
        });

    })
