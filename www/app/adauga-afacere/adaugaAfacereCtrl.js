angular.module('profesionist.controllers')

    .controller('AdaugaAfacereCtrl', function ($scope, $rootScope, $stateParams, Afaceri, currentAuth, $firebaseArray, $firebaseObject) {
        // console.log("AdaugaAfacereCtrl", currentAuth)
        var ref = firebase.database().ref();
        var auth = firebase.auth();
        var afaceriRef = ref.child("afaceri");
        var afaceri = $firebaseArray(afaceriRef);
        var indexAfacereCrt = -1;
        $scope.luni = false;
        $scope.marti = false;
        $scope.miercuri = false;
        $scope.joi = false;
        $scope.vineri = false;
        $scope.sambata = false;
        $scope.duminica = false;
        $scope.afacere = {
            "adresa": "",
            "email": "",
            "nume": "",
            "orar": {
                "luni": {
                    "start": "",
                    "stop": ""
                },
                "marti": {
                    "start": "",
                    "stop": ""
                },
                "miercuri": {
                    "start": "",
                    "stop": ""
                },
                "joi": {
                    "start": "",
                    "stop": ""
                },
                "vineri": {
                    "start": "",
                    "stop": ""
                },
                "sambata": {
                    "start": "",
                    "stop": ""
                },
                "duminica": {
                    "start": "",
                    "stop": ""
                }
            },
            "proprietar": currentAuth.uid,
            "ratings": {
                "nrVoturi": 0,
                "totalVoturi": 0
            },
            "site": "",
            "telefon": ""
        };

        function prepareData(afacere) {
            if (validateProp(afacere, "luni", "start") || validateProp(afacere, "luni", "stop"))
                if ((afacere.orar.luni.start || afacere.orar.luni.stop) && (afacere.orar.luni.start.indexOf(":") > -1 || afacere.orar.luni.stop.indexOf(":") > -1))
                    $scope.luni = true;
            if (validateProp(afacere, "marti", "start") || validateProp(afacere, "marti", "stop"))
                if ((afacere.orar.marti.start || afacere.orar.marti.stop) && (afacere.orar.marti.start.indexOf(":") > -1 || afacere.orar.marti.stop.indexOf(":") > -1))
                    $scope.marti = true;
            if (validateProp(afacere, "miercuri", "start") || validateProp(afacere, "miercuri", "stop"))
                if ((afacere.orar.miercuri.start || afacere.orar.miercuri.stop) && (afacere.orar.miercuri.start.indexOf(":") > -1 || afacere.orar.miercuri.stop.indexOf(":") > -1))
                    $scope.miercuri = true;
            if (validateProp(afacere, "joi", "start") || validateProp(afacere, "joi", "stop"))
                if ((afacere.orar.joi.start || afacere.orar.joi.stop) && (afacere.orar.joi.start.indexOf(":") > -1 || afacere.orar.joi.stop.indexOf(":") > -1))
                    $scope.joi = true;
            if (validateProp(afacere, "vineri", "start") || validateProp(afacere, "vineri", "stop"))
                if ((afacere.orar.vineri.start || afacere.orar.vineri.stop) && (afacere.orar.vineri.start.indexOf(":") > -1 || afacere.orar.vineri.stop.indexOf(":") > -1))
                    $scope.vineri = true;
            if (validateProp(afacere, "sambata", "start") || validateProp(afacere, "sambata", "stop"))
                if ((afacere.orar.sambata.start || afacere.orar.sambata.stop) && (afacere.orar.sambata.start.indexOf(":") > -1 || afacere.orar.sambata.stop.indexOf(":") > -1))
                    $scope.sambata = true;
            if (validateProp(afacere, "duminica", "start") || validateProp(afacere, "duminica", "stop"))
                if ((afacere.orar.duminica.start || afacere.orar.duminica.stop) && (afacere.orar.duminica.start.indexOf(":") > -1 || afacere.orar.duminica.stop.indexOf(":") > -1))
                    $scope.duminica = true;

            $scope.afacere = {
                "adresa": afacere.adresa,
                "email": afacere.email,
                "nume": afacere.nume,
                "orar": {
                    "luni": {
                        "start": validateProp(afacere, "luni", "start") ? setTime(afacere.orar.luni.start) : "",
                        "stop": validateProp(afacere, "luni", "stop") ? setTime(afacere.orar.luni.stop) : ""
                    },
                    "marti": {
                        "start": validateProp(afacere, "marti", "start") ? setTime(afacere.orar.marti.stop) : "",
                        "stop": validateProp(afacere, "marti", "stop") ? setTime(afacere.orar.marti.stop) : ""
                    },
                    "miercuri": {
                        "start": validateProp(afacere, "miercuri", "start") ? setTime(afacere.orar.miercuri.start) : "",
                        "stop": validateProp(afacere, "miercuri", "stop") ? setTime(afacere.orar.miercuri.stop) : ""
                    },
                    "joi": {
                        "start": validateProp(afacere, "joi", "start") ? setTime(afacere.orar.joi.start) : "",
                        "stop": validateProp(afacere, "joi", "stop") ? setTime(afacere.orar.joi.stop) : ""
                    },
                    "vineri": {
                        "start": validateProp(afacere, "vineri", "start") ? setTime(afacere.orar.vineri.start) : "",
                        "stop": validateProp(afacere, "vineri", "stop") ? setTime(afacere.orar.vineri.stop) : ""
                    },
                    "sambata": {
                        "start": validateProp(afacere, "sambata", "start") ? setTime(afacere.orar.sambata.start) : "",
                        "stop": validateProp(afacere, "sambata", "stop") ? setTime(afacere.orar.sambata.stop) : ""
                    },
                    "duminica": {
                        "start": validateProp(afacere, "duminica", "start") ? setTime(afacere.orar.duminica.start) : "",
                        "stop": validateProp(afacere, "duminica", "stop") ? setTime(afacere.orar.duminica.stop) : ""
                    }
                },
                "proprietar": currentAuth.uid,
                "ratings": {
                    "nrVoturi": 0,
                    "totalVoturi": 0
                },
                "site": afacere.site,
                "telefon": afacere.telefon
            };
        }




        $scope.start = function () {
            if ($stateParams.idAfacere != -1) {
                afaceri.$loaded().then(function (afaceri) {
                    indexAfacereCrt = afaceri.$indexFor($stateParams.idAfacere);
                    prepareData(afaceri.$getRecord($stateParams.idAfacere));
                })
            }
        };
        $scope.start();


        $scope.test = "";

        $scope.save = function () {
            var afacereNoua = {
                "adresa": $scope.afacere.adresa,
                "email": $scope.afacere.email,
                "nume": $scope.afacere.nume,
                "orar": {
                    "luni": {
                        "start": getTime($scope.afacere.orar.luni.start),
                        "stop": getTime($scope.afacere.orar.luni.stop)
                    },
                    "marti": {
                        "start": getTime($scope.afacere.orar.marti.stop),
                        "stop": getTime($scope.afacere.orar.marti.stop)
                    },
                    "miercuri": {
                        "start": getTime($scope.afacere.orar.miercuri.start),
                        "stop": getTime($scope.afacere.orar.miercuri.stop)
                    },
                    "joi": {
                        "start": getTime($scope.afacere.orar.joi.start),
                        "stop": getTime($scope.afacere.orar.joi.stop)
                    },
                    "vineri": {
                        "start": getTime($scope.afacere.orar.vineri.start),
                        "stop": getTime($scope.afacere.orar.vineri.stop)
                    },
                    "sambata": {
                        "start": getTime($scope.afacere.orar.sambata.start),
                        "stop": getTime($scope.afacere.orar.sambata.stop)
                    },
                    "duminica": {
                        "start": getTime($scope.afacere.orar.duminica.start),
                        "stop": getTime($scope.afacere.orar.duminica.stop)
                    }
                },
                "proprietar": currentAuth.uid,
                "ratings": {
                    "nrVoturi": 0,
                    "totalVoturi": 0
                },
                "site": $scope.afacere.site,
                "telefon": $scope.afacere.telefon
            };
            saveOrUpdate(afacereNoua);
        }

        function validateProp(obj, day, tip) {
            if (obj.orar && obj.orar[day] && obj.orar[day][tip])
                return true;
            return false;
        }

        function saveOrUpdate(afacere) {
            if (valideazaAfacere(afacere)) {
                if ($stateParams.idAfacere != -1) {
                    afaceri[indexAfacereCrt].adresa = afacere.adresa;
                    afaceri[indexAfacereCrt].email = afacere.email;
                    afaceri[indexAfacereCrt].nume = afacere.nume;
                    afaceri[indexAfacereCrt].orar.luni.start = afacere.orar.luni.start;
                    afaceri[indexAfacereCrt].orar.luni.stop = afacere.orar.luni.stop;
                    afaceri[indexAfacereCrt].orar.marti.start = afacere.orar.marti.start;
                    afaceri[indexAfacereCrt].orar.marti.stop = afacere.orar.marti.stop;
                    afaceri[indexAfacereCrt].orar.miercuri.start = afacere.orar.miercuri.start;
                    afaceri[indexAfacereCrt].orar.miercuri.stop = afacere.orar.miercuri.stop;
                    afaceri[indexAfacereCrt].orar.joi.start = afacere.orar.joi.start;
                    afaceri[indexAfacereCrt].orar.joi.stop = afacere.orar.joi.stop;
                    afaceri[indexAfacereCrt].orar.vineri.start = afacere.orar.vineri.start;
                    afaceri[indexAfacereCrt].orar.vineri.stop = afacere.orar.vineri.stop;
                    afaceri[indexAfacereCrt].orar.sambata.start = afacere.orar.sambata.start;
                    afaceri[indexAfacereCrt].orar.sambata.stop = afacere.orar.sambata.stop;
                    afaceri[indexAfacereCrt].orar.duminica.start = afacere.orar.duminica.start;
                    afaceri[indexAfacereCrt].orar.duminica.stop = afacere.orar.duminica.stop;
                    afaceri[indexAfacereCrt].site = afacere.site;
                    afaceri[indexAfacereCrt].telefon = afacere.telefon;
                    afaceri.$save(indexAfacereCrt).then(function (ref) {
                        ref.key === afaceri[indexAfacereCrt].$id;
                    });
                }
                else {
                    afaceri.$add(afacere).then(function (ref) {
                        var id = ref.key;
                        afaceri.$indexFor(id); // returns location in the array
                    })
                }
            } else { alert("Eroare la salvare date! Completati campurile obligatorii!") }
        }



        function getTime(data) {
            if (data) {
                var h = new Date(data).getHours().toString();
                var m = new Date(data).getMinutes().toString();
                console.log("get time", h + ":" + m + "?");
                return h + ":" + m;
            }
            else
                return "";
        }

        function setTime(time) {
            var h = time.slice(0, time.indexOf(":"));
            var m = time.slice(time.indexOf(":") + 1, time.length);
            var data = new Date();
            data.setHours(h, m, 0, 0);
            return data;
        }

        function valideazaAfacere(afacere) {
            if (afacere && afacere.nume && afacere.email && afacere.adresa && afacere.telefon)
                return true;
            else return false;
        }


    })
