angular.module('profesionist.controllers')

    .controller('MyAfaceriCtrl', function ($scope, $rootScope, Afaceri, $ionicPopup, $state, $ionicSideMenuDelegate, currentAuth, $firebaseArray, afacerileCurente, $ionicPopover, $timeout) {
        $scope.openMenu = function () {
            $ionicSideMenuDelegate.toggleLeft();
        }
        var ref = firebase.database().ref();
        var auth = firebase.auth();
        var afaceriRef = ref.child("afaceri");
        var afaceri = $firebaseArray(afaceriRef);
        afacerilemele = [];
        $scope.afacerilemele = [];
        $scope.selected;
        afacerileCurente.on("child_added", function (snapshot) {
            afacerilemele.push(addKeyToAfacere(snapshot.val(), snapshot.key));
            $scope.afacerilemele = afacerilemele;
        });

        $scope.getClass = function () {
            return "bg_info";
        }

        $scope.onEdit = function (selected) {
            console.log(selected);
            $state.go('edit-afacere', { "idAfacere": selected.key });
            $scope.popover.hide();
        }

        $scope.onGestionare = function (selected) {
            console.log(selected);
            $state.go('app.programari', { "idAfacere": selected.key });
            $scope.popover.hide();
        }

        $scope.onSelected = function (item) {
            $scope.selected = item;
            for (var i = 0; i < afacerilemele.length; i++)
                if (item.nume === afacerilemele[i].nume && afacerilemele[i].class && afacerilemele[i].class == "bg_info") {
                    afacerilemele[i].class = "";
                    $scope.selected = undefined;
                }
                else if (item.nume === afacerilemele[i].nume) {
                    afacerilemele[i].class = "bg_info";
                }
                else
                    afacerilemele[i].class = "";

        }

        $scope.doRefresh = function () {
            $scope.afacerilemele = [];
            afacerilemele = [];
            Afaceri.getMyAfaceri(currentAuth.uid).on("child_added", function (snapshot) {
                afacerilemele.push(addKeyToAfacere(snapshot.val(), snapshot.key));
                $scope.afacerilemele = afacerilemele;
            });
            $scope.$broadcast('scroll.refreshComplete');
        };

        function addKeyToAfacere(afacere, key) {
            afacere["key"] = key;
            return afacere;
        }

        $ionicPopover.fromTemplateUrl('app/afacerile-mele/popoverDetalii.html', {
            scope: $scope,
        }).then(function (popover) {
            $scope.popover = popover;
        });

    })