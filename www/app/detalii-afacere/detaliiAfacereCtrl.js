angular.module('profesionist.controllers')

    .controller('DetaliiAfacereCtrl', function ($scope, $stateParams, Afaceri, $q, $state, $ionicLoading) {
        $scope.afacere;
        $scope.idAfacere = $stateParams.idAfacere;

        $scope.goToProgramari = function () {
            // $ionicLoading.show({
            //     template: 'Se încarcă...'
            // });
            $state.go('app.programari', { "idAfacere": $stateParams.idAfacere });
        }

        Afaceri.getItem($stateParams.idAfacere).then(function (res) {
            $scope.afacere = res;
            // console.log("detalii", $scope.afacere);

            $scope.editAfacere = function (id) {
                // console.log($stateParams.idAfacere);
                $state.go('adauga-afacere', {
                    idAfacere: $stateParams.idAfacere
                });
            };
        });
    })
