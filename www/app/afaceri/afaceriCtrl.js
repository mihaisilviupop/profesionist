angular.module('profesionist.controllers')

    .controller('AfaceriCtrl', function ($scope, $rootScope, Afaceri, $ionicPopup, $state, $ionicSideMenuDelegate) {
        $scope.openMenu = function () {
            $ionicSideMenuDelegate.toggleLeft();
        }


        $scope.searchKey = "";

        $scope.clearSearch = function () {
            $scope.searchKey = "";
            $scope.afaceri = Afaceri.allItems();
        }

        /**cauta in baza de date textul introdus in input */
        $scope.search = function () {
            // console.log($scope.afaceri);
            if ($scope.searchKey)
                Afaceri.filtruNume($scope.searchKey).then(function (res) {
                    console.log("res", res);
                    $scope.afaceri = res.data;
                });
        }

        /** variabila in care se pastreaza lista afacerilor */
        $scope.afaceri = Afaceri.allItems();
        
        /**functie pentru reimpospatarea listei la pull-down */
        $scope.doRefresh = function () {
            console.log('Refreshing!');
            $scope.afaceri = Afaceri.allItems();
            //Stop the ion-refresher from spinning
            $scope.$broadcast('scroll.refreshComplete');
        };
    })