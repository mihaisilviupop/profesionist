angular.module('profesionist.controllers', [])

    .controller('AppCtrl', function ($scope, $ionicModal, $state, $firebaseAuth, $ionicLoading, $rootScope, $ionicPopup, Utilizatori) {

        // With the new view caching in Ionic, Controllers are only called
        // when they are recreated or on app start, instead of every page change.
        // To listen for when this page is active (for example, to refresh data),
        // listen for the $ionicView.enter event:
        //$scope.$on('$ionicView.enter', function(e) {
        //});
        var ref = firebase.database().ref();
        var auth = firebase.auth();


        $scope.addAfacere = function () {
            if ($rootScope.authData && $rootScope.authData.uid) {
                var confirmPopup = $ionicPopup.confirm({
                    title: 'Adauga afacerea ta!',
                    template: 'Doriti sa adaugati o afacere?'
                });
                confirmPopup.then(function (res) {
                    if (res) {
                        console.log('You are sure');
                        $state.go('edit-afacere', { "idAfacere": -1 });
                    } else {
                        console.log('You are not sure');
                    }
                });
            }
            else {
                //alert ca nu este logat
            }
        };

        // Form data for the login modal
        $scope.loginData = {};

        // Create the login modal that we will use later
        $ionicModal.fromTemplateUrl('templates/login.html', {
            scope: $scope
        }).then(function (modal) {
            $scope.modal = modal;
        });

        // Triggered in the login modal to close it
        $scope.closeLogin = function () {
            $scope.modal.hide();
        };

        // Open the login modal
        $scope.login = function () {
            $scope.modal.show();
        };

        // Perform the login action when the user submits the login form
        $scope.doLogin = function () {
            if ($scope.loginData && $scope.loginData.email && $scope.loginData.password) {
                $ionicLoading.show({
                    template: 'Signing In...'
                });
                auth.signInWithEmailAndPassword($scope.loginData.email, $scope.loginData.password).then(function (authData) {
                    $rootScope.authData = authData;
                    console.log("$rootScope.authData", $rootScope.authData);
                    ref.child("users").child(authData.uid).once('value', function (snapshot) {
                        var val = snapshot.val();
                        $scope.$apply(function () {
                            $rootScope.userData = val;
                            console.log("$rootScope.userData", $rootScope.userData);

                        });
                    });
                    $ionicLoading.hide();
                    $scope.closeLogin();
                    $state.go('app.afaceri');
                }).catch(function (error) {
                    alert("Eroare la autentificare:" + error.message);
                    $rootScope.userData = undefined;
                    $ionicLoading.hide();
                    $scope.closeLogin();
                });
            } else
                alert("Va rugam completati emailul si parola!");
        };

        // //creare user
        $ionicModal.fromTemplateUrl('templates/signup.html', {
            scope: $scope
        }).then(function (modal) {
            $scope.modalNewUser = modal;
        });

        // Triggered in the login modal to close it
        $scope.closeNewUser = function () {
            $scope.modalNewUser.hide();
        };

        // Open the login modal
        $scope.newUser = function () {
            $scope.modalNewUser.show();
        };

        $scope.createUser = function (user) {
            console.log("Functie de adaugare user apelata");
            if (user && user.email && user.password && user.displayName) {
                $ionicLoading.show({
                    template: 'Inregistrare...'
                });

                auth.createUserWithEmailAndPassword(user.email, user.password).then(function (userData) {
                    alert("Utilizator creat cu succes!");
                    ref.child("users").child(userData.uid).set({
                        email: user.email,
                        displayName: user.displayName,
                        roles: {
                            proprietar: user.companie ? true : false
                        }
                    });
                    $ionicLoading.hide();
                    $scope.modalNewUser.hide();
                }).catch(function (error) {
                    alert("Error: " + error);
                    $ionicLoading.hide();
                });
            } else
                alert("Completeaza toate detaliile");
        }

    })

    .controller('SetariCtrl', function ($scope, $ionicModal, $state, $firebaseAuth, $ionicLoading, $rootScope, Utilizatori, currentAuth) {
        console.log('Setari Controller Initialized', currentAuth);

        var ref = firebase.database().ref();
        var auth = firebase.auth();
        $scope.displayName = $rootScope.authData ? $rootScope.authData.displayName : undefined;
        console.log("userData from SetariCtrl", $rootScope.userData);

        //creare user
        $ionicModal.fromTemplateUrl('templates/signup.html', {
            scope: $scope
        }).then(function (modal) {
            $scope.modalNewUser = modal;
        });

        // Triggered in the login modal to close it
        $scope.closeNewUser = function () {
            $scope.modalNewUser.hide();
        };

        // Open the login modal
        $scope.newUser = function () {
            $scope.modalNewUser.show();
        };

        $scope.createUser = function (user) {
            console.log("Functie de adaugare user apelata");
            if (user && user.email && user.password && user.displayName) {
                $ionicLoading.show({
                    template: 'Inregistrare...'
                });

                auth.createUserWithEmailAndPassword(user.email, user.password).then(function (userData) {
                    alert("Utilizator creat cu succes!");
                    ref.child("users").child(userData.uid).set({
                        email: user.email,
                        displayName: user.displayName,
                        roles: {
                            proprietar: user.companie ? true : false
                        }
                    });
                    $ionicLoading.hide();
                    $scope.modalNewUser.hide();
                }).catch(function (error) {
                    alert("Error: " + error);
                    $ionicLoading.hide();
                });
            } else
                alert("Completeaza toate detaliile");
        }

        $scope.updateProfile = function (user) {
            if ($rootScope.authData.uid && user.email && user.displayName) {
                Utilizatori.setUser($rootScope.authData.uid, user);
            }
            else {
                alert("Completeaza toate câmpurile!");
            }
        }


    });

