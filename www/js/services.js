angular.module('profesionist.services', ['ngResource', 'firebase'])

    .factory("Programari", function ($firebaseArray, $firebaseObject) {
        var userReference = firebase.database().ref();
        var ref = firebase.database().ref();
        return {

        };
    })

    .factory("Auth", ["$firebaseAuth",
        function ($firebaseAuth) {
            return $firebaseAuth();
        }
    ])

    .factory('Afaceri', function ($firebaseArray, $http) {
        var afaceriReference = firebase.database().ref("afaceri");
        return {
            allItems: function () {
                return $firebaseArray(afaceriReference);
            },
            filtruNume: function (nume) {
                console.log("search key", nume);
                // var ref = new Firebase("https://programari.firebaseio.com/afaceri");
                // console.log("ref", nume);
                // ref.orderByChild("nume").equalTo(nume).on("value", function(exec) {
                //     exec.forEach(function(data) {
                //         console.log("The " + data.key() + " dinosaur's score is " + data.val());
                //     });
                // });
                console.log("get", $http.get('https://programari.firebaseio.com/afaceri.json?orderBy="nume"&equalTo="' + nume + '"&print=pretty'));
                return $http.get('https://programari.firebaseio.com/afaceri.json?orderBy="nume"&equalTo="' + nume + '"&print=pretty');
            },
            getItem: function (id) {
                var afaceriArray = $firebaseArray(afaceriReference);
                return afaceriArray.$loaded().then(function (afaceriArray) {
                    return afaceriArray.$getRecord(id);
                })
            },
            setItem: function (id) {
                //console.log(id, afaceriArray.$indexFor(id.$id), afaceriArray.$getRecord(id), afacereCurenta);

            },
            getMyAfaceri: function (uid) {
                //ref.child('users').orderByChild('name').equalTo('Alex').on('child_added',  ...)
                return afaceriReference.orderByChild("proprietar").equalTo(uid);
            }
        };
    })

    .factory("Utilizatori", function ($firebaseArray, $http, $ionicLoading, $ionicModal) {
        var userReference = firebase.database().ref("users");
        var ref = firebase.database().ref()
        return {
            getUser: function (id) {
                return $http.get('https://programari.firebaseio.com/users.json?orderBy="$key"&equalTo="' + id + '"&print=pretty');
            },
            setUser: function (uid, user) {
                if (uid, user) {
                    ref.child("users").child(uid).set({
                        email: user.email,
                        displayName: user.displayName,
                        roles: {
                            proprietar: user.roles.proprietar ? true : false
                        }
                    });
                }
            }
        };
    })

