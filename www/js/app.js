
/**  
 * Comanda angular.module este un spatiu global pentru crearea, inregistrarea si preluarea modulelor Angular
 * Toate modulele, atat cele din Angular Core cat si cele de la parti terte, 
 * care ar trebui sa fie disponibile unei aplicatii, trebuie sa fie inregistrate folosind acest mecanism.
 * Cand cand se apeleaza cu doi sau mai multi parametrii, este creat un nou modul. 
 * Daca este apelat cu un singur parametru, atunci un modul existent este preluat.
 * 'profesionist' reprezinta numele modulului creat pentru aplicatia de fata 
 *    nume care se regaseste si in atributul etichetei <body> in fisierul index.html
 *  al doilea parametru este o multime a dependintelor 
 * 'profesionist.controllers' se gaseste in controllers.js
 * 'profesionist.services' se gaseste in services.js
 * */
angular.module('profesionist', ['ionic', 'firebase', 'profesionist.controllers', 'profesionist.services'])
  // .config(function () {
  //   var config = {
  //     apiKey: "AIzaSyDFqWRdxb08ei33rU3gzaXTJYP9tFzlfSU",
  //     authDomain: "programari.firebaseapp.com",
  //     databaseURL: "https://programari.firebaseio.com",
  //     storageBucket: "project-8014281663332581080.appspot.com",
  //   };
  //   firebase.initializeApp(config);
  // })
  .run(function ($ionicPlatform, $rootScope, $location, Auth, Utilizatori, $ionicLoading, $firebaseAuth, $state, $ionicModal) {
    $ionicPlatform.ready(function () {
      // ascunde bara de accesori cand este afisata tastatura
      if (window.cordova && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      }
      if (window.StatusBar) {
        StatusBar.styleDefault();
      }

      var ref = firebase.database().ref();
      var auth = firebase.auth();

      $rootScope.firebaseUrl = firebase.database().ref();;
      $rootScope.displayName = null;

      /**Functie apelata de fiecare data cand se schimba starea autentificarii */
      Auth.$onAuthStateChanged(function (user) {
        if (user) {
          // utilizator autentificat
          $rootScope.authData = user;
          Utilizatori.getUser($rootScope.authData.uid).then((user) => {
            $rootScope.userData = user.data[$rootScope.authData.uid];
          })

        } else {
          // utilizator fara autentificare
          $ionicLoading.hide();
          $rootScope.authData = undefined;
          $location.path('/login');
        }
      });


      $rootScope.logout = function () {
        //  console.log("Logging out from the app");
        $ionicLoading.show({
          template: 'Logging Out...'
        });
        Auth.$signOut();
        $rootScope.authData = undefined;
        $ionicLoading.hide();

      }

      $rootScope.loginData = {};


      $ionicModal.fromTemplateUrl('templates/login.html', {
        scope: $rootScope
      }).then(function (modal) {
        $rootScope.modal = modal;
      });

      // Triggered in the login modal to close it
      $rootScope.closeLogin = function () {
        $rootScope.modal.hide();
      };

      // Open the login modal
      $rootScope.login = function () {
        $rootScope.modal.show();
      };

      // Perform the login action when the user submits the login form
      $rootScope.doLogin = function () {
        if ($rootScope.loginData && $rootScope.loginData.email && $rootScope.loginData.password) {
          $ionicLoading.show({
            template: 'Signing In...'
          });
          auth.signInWithEmailAndPassword($rootScope.loginData.email, $rootScope.loginData.password).then(function (authData) {
            $rootScope.authData = authData;
            console.log("$rootScope.authData", $rootScope.authData);
            ref.child("users").child(authData.uid).once('value', function (snapshot) {
              var val = snapshot.val();
              $rootScope.$apply(function () {
                $rootScope.userData = val;
                console.log("$rootScope.userData", $rootScope.userData);
              });
            });
            $ionicLoading.hide();
            $rootScope.closeLogin();
            // $state.go('setari');
          }).catch(function (error) {
            alert("Eroare la autentificare:" + error.message);
            $rootScope.userData = undefined;
            $ionicLoading.hide();
            $rootScope.closeLogin();
          });
        } else
          alert("Va rugam completati emailul si parola!");
      };
      $rootScope.currentState = 'app.afaceri';
      $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
        $rootScope.currentState = toState.name;
      });

      $rootScope.$on("$stateChangeError", function (event, toState, toParams, fromState, fromParams, error) {
        // We can catch the error thrown when the $requireAuth promise is rejected
        // and redirect the user back to the home page
        if (error === "AUTH_REQUIRED") {
          $rootScope.login();
        }
        if (error === "nope") {
          console.log(error);
          event.preventDefault();
          $ionicLoading.show({
            template: 'Vă rugăm să vă autentificați',
            duration: 1000
          });
        }
      });

    });
  })

  .config(function ($stateProvider, $urlRouterProvider, $ionicConfigProvider) {
    $ionicConfigProvider.tabs.position('bottom');

    $stateProvider

      .state('app', {
        url: "/app",
        abstract: true,
        templateUrl: "templates/menu.html",
        controller: 'AppCtrl'
      })

      .state('app.browse', {
        url: "/browse",
        views: {
          'tab-browse': {
            templateUrl: "templates/browse.html"
          }
        },
      })

      .state('app.myafaceri', {
        url: '/afacerilemele',
        views: {
          'tab-myafaceri': {
            templateUrl: "app/afacerile-mele/myAfaceri.html",
            controller: 'MyAfaceriCtrl',
            resolve: {
              "currentAuth": function (Auth) {
                return Auth.$requireSignIn();
              },
              "afacerileCurente": function (Afaceri, currentAuth) {
                if (currentAuth && currentAuth.uid)
                  return Afaceri.getMyAfaceri(currentAuth.uid);
                //  .on("child_added", function (snapshot) {
                //     return snapshot;
                //   });
              }
            }
          }
        }
      })

      .state('setari', {
        url: "/setari",
        templateUrl: "templates/setari.html",
        controller: 'SetariCtrl',
        resolve: {
          "currentAuth": ["Auth", function (Auth) {
            return Auth.$requireSignIn();
          }]
        }
      })

      .state('app.afaceri', {
        url: '/afaceri',
        views: {
          'tab-afaceri': {
            templateUrl: "app/afaceri/afaceri.html",
            controller: 'AfaceriCtrl'
          }
        }
      })

      .state('app.detalii', {
        url: '/afaceri/:idAfacere',
        views: {
          'tab-afaceri': {
            templateUrl: 'app/detalii-afacere/detaliiAfacere.html',
            controller: 'DetaliiAfacereCtrl'
          }
        }
      })

      .state('app.programari', {
        url: '/programari/:idAfacere',
        views: {
          'tab-afaceri': {
            templateUrl: 'app/programari/programari.new.html',
            controller: 'ProgramariNewCtrl',
            resolve: {
              "currentAuth": function (Auth, $stateParams) {
                return Auth.$requireSignIn();
              },
              "currentAfacere": function (Afaceri, $stateParams) {
                return Afaceri.getItem($stateParams.idAfacere);
              }
            }
          }
        }
      })


      .state('programarilist', {
        url: "/programari-list",
        templateUrl: "app/programari/programari.list.html",
        controller: 'ProgramariListCtrl',
        resolve: {
          "currentAuth": ["Auth", function (Auth) {
            return Auth.$requireSignIn();
          }]
        }
      })

      //:idAfacere
      .state('edit-afacere', {
        url: '/afacere/edit/:idAfacere',
        templateUrl: 'app/adauga-afacere/adaugaAfacere.html',
        controller: 'AdaugaAfacereCtrl',
        resolve: {
          "currentAuth": ["Auth", function (Auth) {
            return Auth.$requireSignIn();
          }]
        }
      })


    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/app/afaceri');
  });
