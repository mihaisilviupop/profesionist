Utilizator:
- pagina toate programarile 
	- vizualizare status programari
	- anulare programare
data aleasa verific ziua saptamanii si orarul de functionare din ziua respectiva
aduc programarile din ziua respectiva
time picker cu pas de 15 sau 30 min
8:00 - 8:30
8:30 - 9:00
8:00 - 8:30
8:00 - 8:30
etc.
aduc data start din ziua respectiva incrementez cu cate o jumatate de ora pana la data stop
data start n - n+30
n+30 - n+60 ..
n+m - data stop  




- pagina rezervare
	- selectare data
	- vizualizare zi cu orele disponibile
	- selectare ora dorita pentru programare
		- introducere mesaj, nume se completeaza automat, telefon se completeaza automat
		- trimitere programare cu status asteptare


Companie:
- pagina cu toate programarile neconfirmate
	- acceptare sau anulare programare

- pagina planificare
	- selectare data
	- vizualizare programari din ziua respectiva cu statusul lor
		- modificare status programare


STATUS REZERVARi 
asteptare, acceptat, respins

"rezervari-1"
------------
-con: prea multe noduri si nivele, greu sa iau rezervarile unui user
-pro: usor sa iau rezervarile unei afaceri, durata dinamica la o rezervare

"rezervari-2"
------------
-con: fara ora stop, presupun ca toate dureaza 30 min
-pro: intindere pe orizontala, aduc mai usor rezervarile unui user, sortare pe server a rezervarilor


user: alege o afacere, o zi, se aduce din bd rezervarile din ziua respectiva, se alege ora, se inregistreaza in bd in asteptarea statusului
afacere: alege o zi vizualizeaza rezervarile, schimba statusul